//
//  main.cpp
//  GLGraphicsBookSample
//
//  Created by Efrain Astudillo on 8/18/14.
//  Copyright (c) 2014 Efrain Astudillo. All rights reserved.
//

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <GL/glew.h>
//#include <OpenGL/OpenGL.h>  // since there is included glew.h
#include <GLUT/GLUT.h>
class Polygon{
public:
  
};
int mWidth = 800;
int mHeight = 600;

typedef GLint vertex3 [3];

vertex3  points[6] = { {20,40,30}, {40,20,30}, {70,20,30}, {90,40,30}, {70,60,30}, {40,60,30} };
vertex3 triangles[6] = { {20,20,-130}, {50,60,-130}, {80,20,-130}, {60,20,-130}, {90,60,-130}, {120,20,-130} };
GLuint list;

const char* vertext_source =  "attribute vec2 coords;"
                              "void main(void){"
                              " gl_Position = vec4(coords, 0.0, 1.0);"
                              "}";
const char* fragment_source = "void main(){"
                                "gl_FragColor[0] = gl_FragCoord.x/640.0;"
                                "gl_FragColor[1] = gl_FragCoord.y/480.0;"
                                "gl_FragColor[2] = 0.5;"
                              "}";
GLuint program;
GLint attribute_coord2d;
GLboolean blend_status = false;

void OnInit(){
  GLuint fsv =
  glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(fsv, 1, &vertext_source, NULL);
  glCompileShader(fsv);
  
  GLuint fsf =
  glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fsf, 1, &fragment_source,NULL);
  glCompileShader(fsf);
  
  GLint status_v;
  GLint status_f;
  GLint status_link;
  
  glGetShaderiv(fsv,GL_COMPILE_STATUS, &status_v);
  glGetShaderiv(fsf, GL_COMPILE_STATUS, &status_f);
  
  if (!status_v) {
    std::cout<< "Error with Vertex shader"<< std::endl;
  }
  if (!status_f) {
    std::cout<< "Error with Fragment shader"<<std::endl;
  }
  
  program = glCreateProgram();
  glAttachShader(program, fsv);
  glAttachShader(program, fsf);
  glLinkProgram(program);
  glGetProgramiv(program, GL_LINK_STATUS, &status_link);
  
  if (!status_link) {
    std::cout << " Error compiling a program " <<std::endl;
  }
  
  const char* attribute_name = "coords";
  attribute_coord2d = glGetAttribLocation(program, attribute_name);
  if (attribute_coord2d == -1) {
    fprintf(stderr, "Could not bind attribute %s\n", attribute_name);
  }
  
  glEnable (GL_BLEND);// enable transparency
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // enable transparency
                                                      // factores de fundido de origen y destino
  glShadeModel (GL_SMOOTH);
  
  glClearColor(0.6f, 0.7f, 0.7f, 1.0f);// set this color to color clear buffer
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  //gluOrtho2D(0.0f, mWidth, 0.0f, mHeight);
  glOrtho(0.0f,mWidth, 0.0f, mHeight, 0.0f, 150.0f);
  // to get what matrix  mode is already set
  int value[1];
  glGetIntegerv(GL_MATRIX_MODE, value);
  std::cout<<"MAtrix: "<<GL_MODELVIEW<<std::endl;
  std::cout<<"Another: "<< value[0]<<std::endl;
  
  list =  glGenLists(1);
  if (list != 0) {
    if(glIsList(list)){
      glNewList(list, GL_COMPILE);
      glColor3f(1.0f, 0.2f, 0.0f);
      glBegin(GL_LINES);
      glVertex2i(10, 10);
      glVertex2i(300, 380);
      glEnd();
      glEndList();
      std::cout<< "list created"<<std::endl;
    }
  }
}
void DrawString(char* str){
  glRasterPos2i((mWidth / 2 ) - 150, mHeight - 20);
  std::string temp(str);
  //std::cout << "size: "<< temp.size()<<std::endl;
  for (int i = 0 ; i < temp.size() ; ++i) {
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, temp.c_str()[i]);
  }
}
void OnDestroy(){
  glDeleteProgram(program);
}
void OnKeyboard(unsigned char key, GLint x, GLint y){
  if (key == 27){ //escape
    exit(EXIT_SUCCESS);
  } else if(key == 'a' || key == 'A'){
    if (!blend_status) {
      glDisable(GL_BLEND);
      blend_status = true;
    }else{
      blend_status = false;
      glEnable(GL_BLEND);
    }
    glutPostRedisplay();
  }
}
void quad(){
  glColor3f(0.9f, 0.5f, 0.2f);
  glBegin(GL_POLYGON);
  glVertex3iv(points[0]);
  glVertex3iv(points[1]);
  glVertex3iv(points[2]);
  glVertex3iv(points[3]);
  glVertex3iv(points[4]);
  glVertex3iv(points[5]);
  glEnd();
  //
  glColor3f(0.7f, 0.5f, 0.0f);
  glBegin(GL_LINE_STRIP);
  glVertex3iv(points[0]);
  glVertex3iv(points[1]);
  glVertex3iv(points[5]);
  glVertex3iv(points[2]);
  glVertex3iv(points[4]);
  glVertex3iv(points[3]);
  glEnd();
  //isolate triangles
  glColor3f(0.7f, 0.5f, 0.0f);
  glBegin(GL_TRIANGLES);
  glVertex3iv(points[0]);
  glVertex3iv(points[5]);
  glVertex3iv(points[1]);
  glVertex3iv(points[2]);
  glVertex3iv(points[3]);
  glVertex3iv(points[4]);
  glEnd();
}
void OnReshape(GLint width, GLint height){
  glViewport(0, 0, width, height);
  glutPostRedisplay();
}
void OnDisplay(){
  glClear(GL_COLOR_BUFFER_BIT); //enable color buffer to set the color for clear

  //glUseProgram(program);
  /*
  glEnableVertexAttribArray(attribute_coord2d);
  GLfloat triangle_vertices[] = {
    0.0,  0.8,
    -0.8, -0.8,
    0.8, -0.8,
  };
   //Describe our vertices array to OpenGL (it can't guess its format automatically)
  glVertexAttribPointer(
                        attribute_coord2d, // attribute
                        2,                 // number of elements per vertex, here (x,y)
                        GL_FLOAT,          // the type of each element
                        GL_FALSE,          // take our values as-is
                        0,                 // no extra data between each position
                        triangle_vertices  // pointer to the C array
                        );
  // Push each element in buffer_vertices to the vertex shader
  glDrawArrays(GL_TRIANGLES, 0, 3);
  glDisableVertexAttribArray(attribute_coord2d);
  */
  //glCallList(list);
  //quad();
  //glEnable(GL_BLEND);
  
  glMatrixMode(GL_PROJECTION);
  glBegin(GL_TRIANGLES);
  glColor4f(1.0f, 0.0f, 0.0f,0.5f);
  glVertex3iv(triangles[0]);
  glColor3f(0.3f, 0.1f, 0.4);
  glVertex3iv(triangles[2]);
  
  glVertex3iv(triangles[1]);
  glColor4f(0.0f, 0.0f, 1.0f,0.5f);
  glVertex3iv(triangles[3]);
  glColor3f(1.0f, 0.0f, 0.3f);
  glVertex3iv(triangles[5]);
  glColor3f(0.0f, 0.5f, 0.5f);
  glVertex3iv(triangles[4]);
  glEnd();
  
  glColor3f(1.0f, 1.0f, 1.0f);
  DrawString("Some Opengl Examples from Book");
  glutSwapBuffers(); //change the buffer
}

int main(int argc, char ** argv)
{
  std::string window_name = "SamplesBook";
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitWindowSize(mWidth, mHeight);
  glutCreateWindow(window_name.c_str());

  glutKeyboardFunc(OnKeyboard);
  
  GLenum glew_status = glewInit();
  if (glew_status != GLEW_OK) {
    std::cerr << "Error loading GLEW library " <<
    glewGetErrorString(glew_status) <<std::endl;
    return EXIT_FAILURE;
  }
  
  std::cout<< "Status: Using GLEW "<< glewGetString(GLEW_VERSION)<<std::endl;
  
  if (GLEW_ARB_vertex_shader && GLEW_ARB_fragment_shader) {
    std::cout<< "Ready for GLSL"<<std::endl;
  }
  else{
    std::cout<< "GLSL not supported"<<std::endl;
  }
  OnInit();
  glutDisplayFunc(OnDisplay);
  glutReshapeFunc(OnReshape);
  glutMainLoop();
  
  OnDestroy();
  return 0;
}